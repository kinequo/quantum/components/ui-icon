import { html, render } from '../node_modules/lit-html';

render(html`
  <knq-calendar id="calendar1">
  </knq-calendar>
  <knq-calendar id="calendar2" multiple>
    <div class="Header1" slot="header">
      <button>Anterior</button>
      <div class="header-name">Marzo</div>
      <button>Siguiente</button>
    </div>
  </knq-calendar>
  <knq-calendar id="calendar3">
  </knq-calendar>
`, document.body);

calendar1.cellHeaderTemplate = (index, info) => {
  const weekdays = [
    'L', 'M', 'X', 'J', 'V', 'S', 'D'
  ];
  return html`
    <div class="CellHeader1">
      ${weekdays[index]}
    </div>
  `;
};
calendar1.cellTemplate = (date, info) => {
  return html`
    <div class="Cell1"
      ?disabled="${date.getMonth() != info.month}"
      ?selected="${info.selected}">
      ${info.isToday ? html`
        <b>${date.getDate()}</b>
      ` : html`${date.getDate()}`}
    </div>
  `;
};

calendar2.cellTemplate = (date, info) => {
  const colors = ['red', 'blue', 'deeppink', 'pink'];
  const color = colors[Math.floor(Math.random() * 4)];
  return html`
    <div style="pointer-events: none" class="Cell2"
      ?selected="${info.selected}">
      <header>
        ${date.getDate()}
        <div class="selected-point"></div>
      </header>
      ${Math.random() > 0.5 ? html`
        <div style="background: ${color}" class="job">
          Carmelo
        </div>
      ` : html``}
    </div>
  `;
};
calendar2.cellHeaderTemplate = (index, info) => {
  const weekdays = [
    'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'
  ];
  return html`
    <div class="CellHeader2">
      ${weekdays[index]}
    </div>
  `;
};

calendar3.isDateDisabled = (d) => {
  const weekday = d.getDay();
  return [6, 0].includes(weekday);
}


window.addEventListener('knq-calendar-change', e => {
  console.log(e.target, e.detail);
})