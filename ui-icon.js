
import { html, render } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

let library = {};
let instances = [];

class UIIcon extends HTMLElement {

  static set library(newValue) {
    library = newValue;
    instances.forEach(el => el.render());
  }
  static get library() {
    return library;
  }

  constructor() {
    super();
    this.attachShadow({mode: 'open'});
    this._icon = '';
  }

  connectedCallback() {
    this.render();
    instances.push(this);
  }
  disconnectedCallback() {
    instances = instances.filter(el => el != this);;
  }

  get icon() {
    return this._icon;
  }
  set icon(newValue) {
    this._icon = newValue;
    this.render();
  }

  // Public API

  // PRIVATE
  render() {
    const tmpl = html`
      <style>
        :host {
          display: inline-flex;
          width: 24px;
          height: 24px;
        }
        svg {
          width: 100%;
          height: 100%;
          fill: currentColor;
        }
      </style>
      ${this.icon ? unsafeHTML(library[this.icon]) : ''}
    `;
    render(tmpl, this.shadowRoot);
  }
}
window.UIIcon = UIIcon;
window.customElements.define('ui-icon', UIIcon);
